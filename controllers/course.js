const Course = require("../model/Course");

// Create a new course
module.exports.addCourse = async (user, reqBody) => {
	// Validate if user is admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return (`You have no access`);
	}
}

// Controller method for retreiving all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}else{
		return (`You have no access`);
	}
}





// -------------------------------------------ACTIVITY-----------------------------------
// NUMBER 2
module.exports.archiveCourse = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let archivedCourse = {
			isActive : reqBody.isActive,
			
		}

		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}else{
		return (`You have no access`);
	}
}